package com.apifoot.repositorys;

import com.apifoot.models.Equipe;
import com.apifoot.models.Rencontre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface RencontreRepository extends JpaRepository<Rencontre,Long> {
    @Query("SELECT r FROM Rencontre r WHERE r.date > :currentDate")
    List<Rencontre> findUpcomingMatches(Date currentDate);

    @Query("SELECT r FROM Rencontre r WHERE r.date < :currentDate")
    List<Rencontre> findPastMatches(Date currentDate);

    @Query("SELECT r FROM Rencontre r WHERE r.eqDomicile = :idEquipe OR r.eqExterieur = :idEquipe")
    List<Rencontre> findMatchsByEquipe(Equipe idEquipe);
}
