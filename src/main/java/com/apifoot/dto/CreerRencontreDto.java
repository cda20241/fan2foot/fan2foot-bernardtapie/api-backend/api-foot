package com.apifoot.dto;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Time;
import java.util.Date;

@Data
@NoArgsConstructor
public class CreerRencontreDto {
    private Date date;
    @Temporal(TemporalType.TIME)
    private Time heure;
    private Long idEqDomicile;
    private Long idEqExterieur;
}
