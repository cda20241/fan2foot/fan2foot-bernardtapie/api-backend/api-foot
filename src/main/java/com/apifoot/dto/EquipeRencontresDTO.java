package com.apifoot.dto;

import com.apifoot.models.Rencontre;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
@Data
@NoArgsConstructor

public class EquipeRencontresDTO extends BaseRencontreDto {


    private BaseEquipeDto eqDomicile;
    private BaseEquipeDto eqExterieur;


    public void mapper(Rencontre rencontre){
        super.mapper(rencontre);
        BaseEquipeDto equipe1 = new BaseEquipeDto();
        equipe1.mapper(rencontre.getEqDomicile());
        this.eqDomicile = equipe1;
        BaseEquipeDto equipe2 = new BaseEquipeDto();
        equipe2.mapper(rencontre.getEqExterieur());
        this.eqExterieur = equipe2;
    }
}
