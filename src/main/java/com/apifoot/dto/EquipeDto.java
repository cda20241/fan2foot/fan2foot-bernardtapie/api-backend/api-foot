package com.apifoot.dto;

import com.apifoot.models.Equipe;
import com.apifoot.models.Joueur;
import com.apifoot.models.Rencontre;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class EquipeDto extends BaseEquipeDto {
    private List<BaseJoueurDto> joueurs = new ArrayList<>();
    private List<EquipeRencontresDTO> rencontres = new ArrayList<>();
    public void mapper(Equipe equipe, List<Rencontre> rencontres){
        super.mapper(equipe);
        for (Joueur joueur : equipe.getJoueurs()){
            BaseJoueurDto joueurDto = new BaseJoueurDto();
            joueurDto.mapper(joueur);
            this.joueurs.add(joueurDto);
        }
        for (Rencontre rencontre : rencontres){
            EquipeRencontresDTO newRencontre = new EquipeRencontresDTO();
            newRencontre.mapper(rencontre);
            this.rencontres.add(newRencontre);
        }
    }
}
