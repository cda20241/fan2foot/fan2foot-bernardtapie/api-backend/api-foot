package com.apifoot.dto;

import com.apifoot.models.Rencontre;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.SimpleDateFormat;

@Data
@NoArgsConstructor
public class BaseRencontreDto {
    private Long id;
    @Temporal(TemporalType.DATE)
    private String date;
    private String heure;
    private Integer scoreDomicile;
    private Integer scoreExterieur;

    public void mapper(Rencontre rencontre){
        this.id = rencontre.getId();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = dateFormat.format(rencontre.getDate());
        this.date = formattedDate;
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        String formattedTime = timeFormat.format(rencontre.getHeure());
        this.heure=formattedTime;
        this.scoreDomicile = rencontre.getScoreDomicile();
        this.scoreExterieur = rencontre.getScoreExterieur();
    }
}
