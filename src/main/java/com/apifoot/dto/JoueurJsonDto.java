package com.apifoot.dto;

public class JoueurJsonDto {
    private Long id_joueur;
    private String nom;
    private String prenom;
    private String short_name;
    private String position;
    private Integer numero;
    private Integer age;
    private Float taille;
    private Integer poids;
    private String nationalite;
    private Integer nombre_but;
    private Integer nombre_passe;
    private Integer nombre_match;

    public JoueurJsonDto(Long id_joueur, String nom, String prenom, String short_name, String position, Integer numero, Integer age, Float taille, Integer poids, String nationalite, Integer nombre_but, Integer nombre_passe, Integer nombre_match) {
        this.id_joueur = id_joueur;
        this.nom = nom;
        this.prenom = prenom;
        this.short_name = short_name;
        this.position = position;
        this.numero = numero;
        this.age = age;
        this.taille = taille;
        this.poids = poids;
        this.nationalite = nationalite;
        this.nombre_but = nombre_but;
        this.nombre_passe = nombre_passe;
        this.nombre_match = nombre_match;
    }

    public JoueurJsonDto() {
    }

    public Long getId_joueur() {
        return id_joueur;
    }

    public void setId_joueur(Long id_joueur) {
        this.id_joueur = id_joueur;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Float getTaille() {
        return taille;
    }

    public void setTaille(Float taille) {
        this.taille = taille;
    }

    public Integer getPoids() {
        return poids;
    }

    public void setPoids(Integer poids) {
        this.poids = poids;
    }

    public String getNationalite() {
        return nationalite;
    }

    public void setNationalite(String nationalite) {
        this.nationalite = nationalite;
    }

    public Integer getNombre_but() {
        return nombre_but;
    }

    public void setNombre_but(Integer nombre_but) {
        this.nombre_but = nombre_but;
    }

    public Integer getNombre_passe() {
        return nombre_passe;
    }

    public void setNombre_passe(Integer nombre_passe) {
        this.nombre_passe = nombre_passe;
    }

    public Integer getNombre_match() {
        return nombre_match;
    }

    public void setNombre_match(Integer nombre_match) {
        this.nombre_match = nombre_match;
    }
}
