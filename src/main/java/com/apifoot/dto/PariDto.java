package com.apifoot.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class PariDto {
    private Long id;
    private Float cote_domicile;
    private Float cote_nul;
    private Float cote_exterieur;
}
