package com.apifoot.dto;

import com.apifoot.models.Joueur;
import com.apifoot.models.Rencontre;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
public class RencontreDto extends BaseRencontreDto {
    private EquipeDto eqDomicile;
    private EquipeDto eqExterieur;
    private List<JoueurDto> butsDomicile = new ArrayList<>();
    private List<JoueurDto> butsExterieur = new ArrayList<>();
    private List<JoueurDto> passesDomicile = new ArrayList<>();
    private List<JoueurDto> passesExterieur = new ArrayList<>();
    private PariDto pari;
    public void mapper(Rencontre rencontre, PariDto pari){
        super.mapper(rencontre);
        this.eqDomicile = new EquipeDto();
        this.eqDomicile.mapper(rencontre.getEqDomicile());
        this.eqExterieur = new EquipeDto();
        this.eqExterieur.mapper(rencontre.getEqExterieur());
        for (Joueur joueur : rencontre.getButs()){
            JoueurDto joueurDto = new JoueurDto();
            joueurDto.mapper(joueur);
            if (joueurDto.getEquipe().getId() == this.eqDomicile.getId()){
                this.butsDomicile.add(joueurDto);
            } else if (joueurDto.getEquipe().getId() == this.eqExterieur.getId()) {
                this.butsExterieur.add(joueurDto);
            }
        }
        for (Joueur joueur : rencontre.getPasseD()){
            JoueurDto joueurDto = new JoueurDto();
            joueurDto.mapper(joueur);
            if (joueurDto.getEquipe().getId() == this.eqDomicile.getId()){
                this.passesDomicile.add(joueurDto);
            } else if (joueurDto.getEquipe().getId() == this.eqExterieur.getId()) {
                this.passesExterieur.add(joueurDto);
            }
        }
        this.pari = pari;
    }

}
