package com.apifoot.dto;

import com.apifoot.models.Equipe;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BaseEquipeDto {
    private Long id;

    private String nomLong;

    private String nomCourt;

    private String championnat;

    private Boolean archive;

    public void mapper(Equipe equipe){
        this.id=equipe.getId();
        this.nomLong=equipe.getNomLong();
        this.nomCourt=equipe.getNomCourt();
        this.championnat=equipe.getChampionnat();
        this.archive=equipe.getArchive();
    }

}
