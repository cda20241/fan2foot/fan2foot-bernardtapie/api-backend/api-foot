package com.apifoot.dto;

import com.apifoot.models.Joueur;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class JoueurDto extends BaseJoueurDto {
    private BaseEquipeDto equipe;

    public void mapper(Joueur joueur){
        super.mapper(joueur);
        this.equipe=new BaseEquipeDto();
        this.equipe.mapper(joueur.getEquipe());
    }
}
