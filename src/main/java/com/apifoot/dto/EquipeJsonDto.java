package com.apifoot.dto;

import java.util.List;

public class EquipeJsonDto {
    private Long id_club;
    private String long_name;
    private String short_name;
    private Integer points;
    private List<JoueurJsonDto> liste_joueur;

    public EquipeJsonDto() {
    }

    public EquipeJsonDto(Long id_club, String long_name, String short_name, Integer points, List<JoueurJsonDto> liste_joueur) {
        this.id_club = id_club;
        this.long_name = long_name;
        this.short_name = short_name;
        this.points = points;
        this.liste_joueur = liste_joueur;
    }

    public Long getId_club() {
        return id_club;
    }

    public void setId_club(Long id_club) {
        this.id_club = id_club;
    }

    public String getLong_name() {
        return long_name;
    }

    public void setLong_name(String long_name) {
        this.long_name = long_name;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public List<JoueurJsonDto> getListe_joueur() {
        return liste_joueur;
    }

    public void setListe_joueur(List<JoueurJsonDto> liste_joueur) {
        this.liste_joueur = liste_joueur;
    }
}
