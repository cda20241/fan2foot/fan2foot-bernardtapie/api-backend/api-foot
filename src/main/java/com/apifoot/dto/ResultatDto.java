package com.apifoot.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class ResultatDto {
    private Long idRencontre;
    private List<Long> butsExterieur;
    private List<Long> butsDomicile;
    private List<Long> passesDomicile;
    private List<Long> passesExterieur;
    private Integer scoreDomicile;
    private Integer scoreExterieur;
}
