package com.apifoot.dto;

import com.apifoot.models.Joueur;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BaseJoueurDto {
    private Long id;
    private String nom;
    private String prenom;
    private String nomCourt;
    private String nationalite;
    private Integer age;
    private Integer poids;
    private Float taille;
    private Integer numero;
    private String position;
    private Integer nbButs = 0;
    private Integer nbPasseD = 0;
    private Integer nbMatchs = 0;


    public void mapper(Joueur joueur){
        this.id = joueur.getId();
        this.nom = joueur.getNom();
        this.prenom = joueur.getPrenom();
        this.nomCourt = joueur.getNomCourt();
        this.nationalite = joueur.getNationalite();
        this.age = joueur.getAge();
        this.poids = joueur.getPoids();
        this.taille = joueur.getTaille();
        this.numero = joueur.getNumero();
        this.position = joueur.getPosition();
    }
}
