package com.apifoot.dto;

import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Time;
import java.util.Date;
@Data
@NoArgsConstructor
public class ModifierRencontreDto {
    @Temporal(TemporalType.DATE)
    private Date date;
    private Time heure;
}
