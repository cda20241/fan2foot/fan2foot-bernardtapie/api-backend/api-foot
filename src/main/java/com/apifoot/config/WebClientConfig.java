package com.apifoot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * Configuration de WebClient pour créer des instances de WebClient.
 */
@Configuration
public class WebClientConfig {

    /**
     * Crée et retourne une instance de WebClient.
     *
     * @return Une instance de WebClient configurée.
     */
    @Bean
    public WebClient webClient() {
        return WebClient.create();
    }
}