package com.apifoot.models;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Entity
@NoArgsConstructor
@Table(name = "equipe")
public class Equipe {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @Column(name = "nom_long")
    private String nomLong;

    @Column(name = "nom_court")
    private String nomCourt;

    @Column(name = "championnat")
    private String championnat;

    @Column(name = "archive")
    private Boolean archive;

    @OneToMany(mappedBy = "equipe")
    private List<Joueur> joueurs;

}
