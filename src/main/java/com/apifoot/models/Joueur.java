package com.apifoot.models;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@Table(name = "joueur")
public class Joueur {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "prenom")
    private String prenom;

    @Column(name = "nom_court")
    private String nomCourt;

    @Column(name = "nationalite")
    private String nationalite;

    @Column(name = "age")
    private Integer age;

    @Column(name = "poids")
    private Integer poids;

    @Column(name = "taille")
    private Float taille;

    @Column(name = "numero")
    private Integer numero;

    @Column(name = "position")
    private String position;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "equipe_id")
    private Equipe equipe;
}
