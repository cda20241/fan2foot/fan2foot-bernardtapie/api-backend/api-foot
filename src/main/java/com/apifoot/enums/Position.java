package com.apifoot.enums;

public enum Position {
    GARDIEN,
    DIFFENSEUR,
    MILIEU,
    ATTAQUANT,
}
