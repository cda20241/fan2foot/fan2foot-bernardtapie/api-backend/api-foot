package com.apifoot;

import com.apifoot.repositorys.EquipeRepository;
import com.apifoot.repositorys.JoueurRepository;
import com.apifoot.services.ImportDonnees;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ApiFootApplication implements CommandLineRunner {
	private final EquipeRepository equipeRepository;
	private final JoueurRepository joueurRepository;

	public ApiFootApplication(EquipeRepository equipeRepository, JoueurRepository joueurRepository) {
		this.equipeRepository = equipeRepository;
		this.joueurRepository = joueurRepository;
	}
	@Override
	public void run(String... args) throws Exception {
		ImportDonnees importationDonneesService = new ImportDonnees(equipeRepository, joueurRepository);
		importationDonneesService.creerEquipesJoueurs();
	}
	public static void main(String[] args) {
		SpringApplication.run(ApiFootApplication.class, args);
	}
}
