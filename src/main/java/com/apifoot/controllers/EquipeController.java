package com.apifoot.controllers;

import com.apifoot.dto.BaseEquipeDto;
import com.apifoot.dto.EquipeDto;
import com.apifoot.models.Equipe;
import com.apifoot.services.EquipeService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/equipe")
@RequiredArgsConstructor
@CrossOrigin()
public class EquipeController {
    @Autowired
    private final EquipeService equipeService;

    /** ------------------ ROUTES ------------------------------------- */

    @PostMapping("/creer")
    public ResponseEntity<String> creerEquipe(@RequestBody Equipe equipe){
        return equipeService.creerEquipe(equipe);
    }
    @GetMapping("/liste")
    public List<EquipeDto> afficherListeEquipes(){
        return equipeService.afficherListeEquipes();
    }
    @GetMapping("/equipes_sans_joueurs")
    public List<BaseEquipeDto> listeEquipes(){
        return equipeService.listeEquipesSansJoueurs();
    }
    @GetMapping("/{id}")
    public EquipeDto afficherEquipe(@PathVariable Long id){
        return equipeService.afficherEquipe(id);
    }
    @PutMapping("/modifier/{id}")
    public ResponseEntity<String> modifierEquipe(@PathVariable Long id, @RequestBody Equipe equipe){
        return equipeService.modifierEquipe(id, equipe);
    }
    @DeleteMapping("/supprimer/{id}")
    public ResponseEntity<String> supprimerEquipe(@PathVariable Long id){
        return equipeService.supprimerEquipe(id);
    }
}
