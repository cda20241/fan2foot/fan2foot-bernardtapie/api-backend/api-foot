package com.apifoot.controllers;

import com.apifoot.dto.JoueurDto;
import com.apifoot.models.Joueur;
import com.apifoot.services.JoueurService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/joueur")
@RequiredArgsConstructor
@CrossOrigin()
public class JoueurController {
    @Autowired
    private final JoueurService joueurService;


    /** ------------------ ROUTES ------------------------------------- */

    @PostMapping("/creer")
    public ResponseEntity<String> creerJoueur(@RequestBody Joueur joueur){
        return joueurService.creerJoueur(joueur);
    }
    @GetMapping("/liste")
    public List<JoueurDto> afficherListeJoueurs(){
        return joueurService.afficherListeJoueurs();
    }
    @GetMapping("/{id}")
    public JoueurDto afficherJoueur(@PathVariable Long id){
        return joueurService.afficherJoueur(id);
    }
    @PutMapping("/modifier/{id}")
    public ResponseEntity<String> modifierJoueur(@PathVariable Long id,@RequestBody Joueur joueur){
        return joueurService.modifierJoueur(id, joueur);
    }
    @DeleteMapping("/supprimer/{id}")
    public ResponseEntity<String> supprimerJoueur(@PathVariable Long id){
        return joueurService.supprimerJoueur(id);
    }
}
