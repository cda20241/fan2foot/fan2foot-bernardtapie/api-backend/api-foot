package com.apifoot.controllers;

import com.apifoot.dto.*;
import com.apifoot.services.RencontreService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rencontre")
@RequiredArgsConstructor
@CrossOrigin()
public class RencontreController {
    @Autowired
    private final RencontreService rencontreService;

    /** ------------------ ROUTES ------------------------------------- */

    @PostMapping("/creer")
    public ResponseEntity<String> creerRencontre(@RequestBody CreerRencontreDto rencontre){
        return rencontreService.creerRencontre(rencontre);
    }
    @GetMapping("/liste")
    public List<RencontreDto> afficherListeJoueurs(){
        return rencontreService.afficherListeRencontres();
    }
    @GetMapping("/{id}")
    public RencontreDto afficherRencontre(@PathVariable Long id){
        return rencontreService.afficherRencontre(id);
    }
    @PutMapping("/modifier/{id}")
    public ResponseEntity<String> modifierRencontre(@PathVariable Long id, @RequestBody ModifierRencontreDto rencontre){
        return rencontreService.modifierRencontre(id, rencontre);
    }
    @DeleteMapping("/supprimer/{id}")
    public ResponseEntity<String> supprimerRencontre(@PathVariable Long id){
        return rencontreService.supprimerRencontre(id);
    }

    @PostMapping("/resultat")
    public ResponseEntity<String> ajouterResultat(@RequestBody ResultatDto resultat){
        return rencontreService.ajouterResultatRencontre(resultat);
    }
    @GetMapping("/lancer")
    public ResponseEntity<String> lancerMatchs(){return rencontreService.creerListMatch();}

    @GetMapping("/avenir")
    public List<RencontreDto> avenirMatchs(){return rencontreService.getMatchesAvenir();}

    @GetMapping("/passes")
    public List<RencontreDto> passesMatchs(){return rencontreService.getMatchesPasses();}

}
