package com.apifoot.services;

import com.apifoot.dto.*;
import com.apifoot.models.Equipe;
import com.apifoot.models.Joueur;
import com.apifoot.models.Rencontre;
import com.apifoot.repositorys.EquipeRepository;
import com.apifoot.repositorys.JoueurRepository;
import com.apifoot.repositorys.RencontreRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Service pour la gestion des rencontres entre équipes.
 */
@Service
@RequiredArgsConstructor
@CrossOrigin()
public class RencontreService {
    @Autowired
    private final RencontreRepository rencontreRepository;
    @Autowired
    private final EquipeRepository equipeRepository;
    @Autowired
    private final JoueurRepository joueurRepository;
    @Autowired
    private final WebClient webClient;



    /**
     * Crée une nouvelle rencontre entre deux équipes.
     *
     * @param creerRencontre Les détails de la rencontre à créer.
     * @return ResponseEntity<String> Indiquant le résultat de l'opération.
     */
    public ResponseEntity<String> creerRencontre(CreerRencontreDto creerRencontre){
        try {
            Equipe equipe1 = equipeRepository.findById(creerRencontre.getIdEqDomicile()).orElse(null);
            Equipe equipe2 = equipeRepository.findById(creerRencontre.getIdEqExterieur()).orElse(null);
            // vérifier si les équipes existent
            if (equipe1 == null || equipe2 == null ){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Au moins une des équipes n'existe pas !");
                // vérifier si les équipes sont différentes
            } else if (equipe1 == equipe2){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Une équipe ne peut pas s'affronter à elle même !");
                    }
            //Si tout est bon créer la rencontre
                Rencontre rencontre = new Rencontre();
                rencontre.setDate(creerRencontre.getDate());
                rencontre.setHeure(creerRencontre.getHeure());
                rencontre.setEqDomicile(equipe1);
                rencontre.setEqExterieur(equipe2);
                rencontre.setScoreDomicile(0);
                rencontre.setScoreExterieur(0);
                rencontre.setButs(new ArrayList<>());
                rencontre.setPasseD(new ArrayList<>());
                //Enregitrer une rencontre
                rencontre = rencontreRepository.save(rencontre);
                // Ensuite instancier un pari
                PariDto pariDto = new PariDto();
                // Le pari prend le même ID que rencontre
                pariDto.setId(rencontre.getId());
                // Les contes sont à 1.0 par défaut !
                pariDto.setCote_domicile((float)1.0);
                pariDto.setCote_exterieur((float)1.0);
                pariDto.setCote_nul((float)1.0);
                // Enregistrer un Pari
                pariDto = this.createBet(pariDto);
                // En cas d'erreur :
                if (pariDto == null){
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                            .body("Une erreur s'est produite lors de la création du Pari !");
                }
                return ResponseEntity.ok("La rencontre n° : "+ rencontre.getId() + " et le pari n° : "
                        + pariDto.getId() + " ont été créés avec succès.");
            } catch (Exception e) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .body("Une erreur s'est produite lors de la création de la rencontre.");
            }
    }

    /**
     * Affiche les détails d'une rencontre.
     *
     * @param id L'identifiant de la rencontre.
     * @return RencontreDto Les détails de la rencontre.
     */
    public RencontreDto afficherRencontre(Long id){
        Rencontre rencontre = rencontreRepository.findById(id).orElse(null);
        PariDto pari = this.getOnePari(rencontre.getId());
        // Vérifier si la rencontre existe
        if (rencontre != null){
            RencontreDto rencontreDto = new RencontreDto();
            rencontreDto.mapper(rencontre, pari);
            return rencontreDto;
        }
        return null;
    }

    /**
     * Affiche la liste de toutes les rencontres.
     *
     * @return List<RencontreDto> La liste des rencontres.
     */
    public List<RencontreDto> afficherListeRencontres(){
        List<Rencontre> lesRencontres = rencontreRepository.findAll();
        List<PariDto> paris = this.getParis();

        //Creer une liste de DTO ensuite parcourir les rencontres afin de les mapper
        //Retourner DTO afin d'éviter les problèmes de récursivité
        List<RencontreDto> resultat = new ArrayList<>();
        for (Rencontre rencontre : lesRencontres){
            for (PariDto pari : paris) {
                if (rencontre.getId() == pari.getId()){
                    RencontreDto rencontreDto = new RencontreDto();
                    rencontreDto.mapper(rencontre, pari);
                    resultat.add(rencontreDto);
                }
            }
        }
        return resultat;
    }

    /**
     * Modifie les informations d'une rencontre existante.
     *
     * @param id       L'identifiant de la rencontre à modifier.
     * @param rencontre Les nouvelles informations de la rencontre.
     * @return ResponseEntity<String> Indiquant le résultat de l'opération.
     */
    public ResponseEntity<String> modifierRencontre(Long id, ModifierRencontreDto rencontre){
        try {
            Rencontre newrencontre = rencontreRepository.findById(id).orElse(null);
            // Vérifier si la rencontre existe
            if (newrencontre == null){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Cette rencontre n'existe pas !");
                }
            //si existe passer à la modification de la date et l'heure
            newrencontre.setDate(rencontre.getDate());
            newrencontre.setHeure(rencontre.getHeure());
            rencontreRepository.save(newrencontre);
            return ResponseEntity.ok("La rencontre n° : "+ newrencontre.getId() + " a été modifiée avec succès.");
            }catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Une erreur s'est produite lors de la modification de la rencontre.");
        }

    }

    /**
     * Ajoute un résultat à une rencontre.
     *
     * @param resultat Les détails du résultat à ajouter.
     * @return ResponseEntity<String> Indiquant le résultat de l'opération.
     */
    public ResponseEntity<String> ajouterResultatRencontre(ResultatDto resultat){
        try {
            // récupérer la rencontre en question
            Rencontre rencontre = rencontreRepository.findById(resultat.getIdRencontre()).orElse(null);
            if (rencontre != null) {
                rencontre.setScoreDomicile(resultat.getScoreDomicile());
                rencontre.setScoreExterieur(resultat.getScoreExterieur());
                // Parcourir les tableaux de butteurs afin de créer des buts
                for (Long but : resultat.getButsDomicile()){
                    Joueur joueur = joueurRepository.findById(but).orElse(null);
                    rencontre.getButs().add(joueur);
                }
                for (Long but : resultat.getButsExterieur()){
                    Joueur joueur = joueurRepository.findById(but).orElse(null);
                    rencontre.getButs().add(joueur);
                }
                // Parcourir les tableaux de passeurs afin de créer des passeD
                for (Long passeD : resultat.getPassesDomicile()){
                    Joueur joueur = joueurRepository.findById(passeD).orElse(null);
                    rencontre.getPasseD().add(joueur);
                }
                for (Long passeD : resultat.getPassesExterieur()){
                    Joueur joueur = joueurRepository.findById(passeD).orElse(null);
                    rencontre.getPasseD().add(joueur);
                }
                rencontreRepository.save(rencontre);
                return ResponseEntity.ok("Le résultat a été ajouté avec succès.");
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Cette rencontre n'existe pas dans la base de données.");

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Une erreur s'est produite lors de l'ajout du résultat'");
        }
    }

    /**
     * Supprime une rencontre.
     *
     * @param id L'identifiant de la rencontre à supprimer.
     * @return ResponseEntity<String> Indiquant le résultat de l'opération.
     */
    public ResponseEntity<String> supprimerRencontre(Long id){
        try {
            Rencontre rencontre = rencontreRepository.findById(id).orElse(null);
            // Vérifier si la rencontre existe déjà
            if (rencontre == null){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Cette rencontre n'existe pas'");
            }
            rencontre.getPasseD().clear();
            rencontre.getButs().clear();
            rencontre.setEqDomicile(null);
            rencontre.setEqExterieur(null);
            rencontreRepository.delete(rencontre);
            return ResponseEntity.ok("L'élément a été supprimé avec succès.");
        } catch (Exception e) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Une erreur s'est produite lors de la suppression de l'élément.");
            }
    }

    /**
     * Crée une liste de matchs à partir d'un fichier JSON.
     *
     * @return ResponseEntity<String> Indiquant le résultat de l'opération.
     */
    public ResponseEntity<String> creerListMatch(){
        // lire le fichier JSON
        Resource resource = new ClassPathResource("match.json");
        ObjectMapper objectMapper = new ObjectMapper();
        try (InputStream inputStream = resource.getInputStream()) {
            // extraire les données dans une listes typée
            List<CreerRencontreDto> matchs = Arrays.asList(objectMapper.readValue(inputStream, CreerRencontreDto[].class));
            // Parcourir la liste afin de créer les rencontres
            for (CreerRencontreDto match : matchs){
                this.creerRencontre(match);
            }
            return ResponseEntity.ok("Les matchs on été créés avec succès.");
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Une erreur s'est produite lors de la création des matchs.");
        }
    }

    /**
     * Crée un nouveau pari en envoyant une requête POST à un serveur distant.
     *
     * @param pariDto L'objet PariDto contenant les détails du pari à créer.
     * @return L'objet PariDto représentant le pari nouvellement créé.
     */
    public PariDto createBet(PariDto pariDto) {
        // Envoi d'une requête POST pour créer un nouveau pari
        return webClient.post()
                .uri("http://localhost:8081/pari/new-pari")
                .bodyValue(pariDto) // Attache l'objet PariDto comme corps de la requête
                .retrieve() // Récupération de la réponse
                .bodyToMono(PariDto.class) // Conversion du corps de la réponse en objet PariDto
                .block(); // Blocage jusqu'à ce que la réponse soit reçue et retour du résultat
    }


    /**
     * Récupère une liste de rencontres futures à partir du repository de rencontres.
     *
     * @return Une liste de {@code RencontreDto} représentant les rencontres futures.
     */
    public List<RencontreDto> getMatchesAvenir() {
        List<Rencontre> matchsAvenir = rencontreRepository.findUpcomingMatches(new Date());
        List<PariDto> paris = this.getParis();

        // Créer une liste de DTO puis parcourir les rencontres pour les mapper
        // Retourner DTO pour éviter les problèmes de récursivité
        List<RencontreDto> resultat = new ArrayList<>();
        for (Rencontre rencontre : matchsAvenir){
            for (PariDto pari : paris) {
                if (rencontre.getId() == pari.getId()){
                    RencontreDto rencontreDto = new RencontreDto();
                    rencontreDto.mapper(rencontre, pari);
                    resultat.add(rencontreDto);
                }
            }
        }
        return resultat;
    }

    /**
     * Récupère une liste de rencontres passées à partir du repository de rencontres.
     *
     * @return Une liste de {@code RencontreDto} représentant les rencontres passées.
     */
    public List<RencontreDto> getMatchesPasses() {
        List<Rencontre> matchsPasses = rencontreRepository.findPastMatches(new Date());
        List<PariDto> paris = this.getParis();
        // Créer une liste de DTO puis parcourir les rencontres pour les mapper
        // Retourner DTO pour éviter les problèmes de récursivité
        List<RencontreDto> resultat = new ArrayList<>();
        for (Rencontre rencontre : matchsPasses){
            for (PariDto pari : paris) {
                if (rencontre.getId() == pari.getId()){
                    RencontreDto rencontreDto = new RencontreDto();
                    rencontreDto.mapper(rencontre, pari);
                    resultat.add(rencontreDto);
                }
            }
        }
        return resultat;
    }

    /**
     * Récupère une liste de paris à partir d'une requête GET envoyée à une API distante.
     *
     * @return Une liste de {@code PariDto} représentant les paris récupérés.
     */
    public List<PariDto> getParis() {
        // Envoi d'une requête GET pour récupérer tous les paris
        return webClient.get()
                .uri("http://localhost:8081/pari/get-all")
                //.header(HttpHeaders.AUTHORIZATION, "Bearer " + jwtToken)
                .retrieve() // Récupération de la réponse
                .bodyToFlux(PariDto.class) // Conversion du corps de la réponse en Flux de PariDto
                .collectList() // Conversion du Flux en List
                .block(); // Blocage jusqu'à ce que la réponse soit reçue et retour du résultat
    }
    public PariDto getOnePari(Long id) {
        // Envoi d'une requête GET pour récupérer un pari
        return webClient.get()
                .uri("http://localhost:8081/pari/get-bet-by-id/"+id)
                //.header(HttpHeaders.AUTHORIZATION, "Bearer " + jwtToken)
                .retrieve() // Récupération de la réponse
                .bodyToMono(PariDto.class) // Conversion du corps de la réponse en Mono de PariDto
                .block(); // Blocage jusqu'à ce que la réponse soit reçue et retour du résultat
    }

}
