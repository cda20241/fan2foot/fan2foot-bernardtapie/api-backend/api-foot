package com.apifoot.services;

import com.apifoot.dto.BaseEquipeDto;
import com.apifoot.dto.BaseJoueurDto;
import com.apifoot.dto.EquipeDto;
import com.apifoot.dto.JoueurDto;
import com.apifoot.models.Equipe;
import com.apifoot.models.Rencontre;
import com.apifoot.repositorys.EquipeRepository;

import com.apifoot.repositorys.RencontreRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Service pour la gestion des équipes.
 */
@Service
@RequiredArgsConstructor
public class EquipeService {

    @Autowired
    private final EquipeRepository equipeRepository;
    @Autowired
    private final RencontreRepository rencontreRepository;
    @Autowired
    private final StatistiqueService statistiqueService;

    /**
     * Crée une nouvelle équipe.
     *
     * @param equipe L'équipe à créer.
     * @return ResponseEntity<String> Indiquant le résultat de l'opération.
     */
    public ResponseEntity<String> creerEquipe(Equipe equipe){
        try {
            if (equipe.getId()==null){
                equipe.setArchive(false);
                equipeRepository.save(equipe);
                return ResponseEntity.ok("L'équipe' "+ equipe.getNomLong() + " a été créée avec succès.");
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Cett équipe contient un ID, il se peut qu'elle existe déjà !");
        }catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Une erreur s'est produite lors de la création de l'équipe.");
        }
    }

    /**
     * Affiche les détails d'une équipe.
     *
     * @param id L'identifiant de l'équipe.
     * @return EquipeDto Les détails de l'équipe.
     */
    public EquipeDto afficherEquipe(Long id){
        Equipe equipe = equipeRepository.findById(id).orElse(null);
        if (equipe!=null){
            /** utilisation de DTO pour éviter les problème de recursivité */
            EquipeDto equipeDto = new EquipeDto();
            //récupérer les matchs par équipe
            List<Rencontre> lesRencontres = rencontreRepository.findMatchsByEquipe(equipe);
            //mapper l'équipeDto avec la liste des matchs
            equipeDto.mapper(equipe, lesRencontres);
            for (BaseJoueurDto joueur : equipeDto.getJoueurs()){
                Integer nbButs = statistiqueService.countButsParJoueur(joueur.getId());
                joueur.setNbButs(nbButs);
                Integer nbPasseD = statistiqueService.countPasseDparJoueur(joueur.getId());
                joueur.setNbPasseD(nbPasseD);
                Integer nbMatchs = statistiqueService.countMatchParJoueur(equipeDto.getId());
                joueur.setNbMatchs(nbMatchs);
            }
            return equipeDto;
        }
        return null;
    }

    /**
     * Affiche la liste de toutes les équipes.
     *
     * @return List<EquipeDto> La liste des équipes.
     */
    public List<EquipeDto> afficherListeEquipes(){
        List<Equipe> lesEquipes = equipeRepository.findAll();
        List<EquipeDto> resultat = new ArrayList<>();
        for (Equipe eq : lesEquipes){
            //vérifier si l'équipe n'est pas archivée
            if (eq.getArchive()==false){
                /** utilisation de DTO pour éviter les problème de recursivité */
                EquipeDto equipe = new EquipeDto();
                equipe.mapper(eq);
                resultat.add(equipe);
            }
        }
        return resultat;
    }

    /**
     * Affiche la liste des équipes sans les détails des joueurs.
     *
     * @return List<BaseEquipeDto> La liste des équipes sans joueurs.
     */
    public List<BaseEquipeDto> listeEquipesSansJoueurs(){
        List<Equipe> lesEquipes = equipeRepository.findAll();
        List<BaseEquipeDto> resultat = new ArrayList<>();
        for (Equipe eq : lesEquipes) {
            //vérifier si l'équipe n'est pas archivée
            if (eq.getArchive()==false){
                //passer par la base dto pour retirer les joueurs
                BaseEquipeDto equipe = new BaseEquipeDto();
                equipe.mapper(eq);
                resultat.add(equipe);
            }
        }
        return resultat;
    }

    /**
     * Modifie les informations d'une équipe existante.
     *
     * @param id     L'identifiant de l'équipe à modifier.
     * @param equipe Les nouvelles informations de l'équipe.
     * @return ResponseEntity<String> Indiquant le résultat de l'opération.
     */
    public ResponseEntity<String> modifierEquipe(Long id, Equipe equipe){
        try {
            Equipe newequipe = equipeRepository.findById(id).orElse(null);
            if (newequipe!=null){
                equipe.setId(id);
                equipe.setArchive(false);
                equipeRepository.save(equipe);
                return ResponseEntity.ok("L'équipe' "+ equipe.getNomLong() + " a été modifiée avec succès.");
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Cett équipe n'existe pas !");
        }catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Une erreur s'est produite lors de la modification de l'équipe.");
        }
    }

    /**
     * Supprime une équipe.
     *
     * @param id L'identifiant de l'équipe à supprimer.
     * @return ResponseEntity<String> Indiquant le résultat de l'opération.
     */
    public ResponseEntity<String> supprimerEquipe(Long id){
        try {
            Equipe equipe = equipeRepository.findById(id).orElse(null);
            equipe.setArchive(true);
            equipeRepository.save(equipe);
            return ResponseEntity.ok("L'élément a été supprimé avec succès.");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Une erreur s'est produite lors de la suppression de l'élément.");
        }
    }
}