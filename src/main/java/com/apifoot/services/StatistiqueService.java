package com.apifoot.services;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * Service pour la gestion des statistiques des joueurs et des équipes.
 */
@Service
@RequiredArgsConstructor
public class StatistiqueService {
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Compte le nombre de buts marqués par un joueur.
     *
     * @param idJoueur L'identifiant du joueur.
     * @return int Le nombre de buts marqués par le joueur.
     */
    public int countButsParJoueur(Long idJoueur) {
        String sqlQuery = "SELECT COUNT(id_joueur) as nbButs " +
                "FROM but " +
                "WHERE id_joueur = :idJoueur";

        Query query = entityManager.createNativeQuery(sqlQuery);
        query.setParameter("idJoueur", idJoueur);

        Object result = query.getSingleResult();

        // Conversion du résultat en int
        int nbButs = ((Number) result).intValue();

        return nbButs;
    }

    /**
     * Compte le nombre de passes décisives effectuées par un joueur.
     *
     * @param idJoueur L'identifiant du joueur.
     * @return int Le nombre de passes décisives effectuées par le joueur.
     */
    public int countPasseDparJoueur(Long idJoueur) {
        String sqlQuery = "SELECT COUNT(id_joueur) as nbPasseD " +
                "FROM passed " +
                "WHERE id_joueur = :idJoueur";

        Query query = entityManager.createNativeQuery(sqlQuery);
        query.setParameter("idJoueur", idJoueur);

        Object result = query.getSingleResult();

        // Conversion du résultat en int
        int nbPasseD = ((Number) result).intValue();

        return nbPasseD;
    }

    /**
     * Compte le nombre de matchs auxquels une équipe a participé.
     *
     * @param idEquipe L'identifiant de l'équipe.
     * @return int Le nombre de matchs auxquels l'équipe a participé.
     */
    public int countMatchParJoueur(Long idEquipe) {
        String sqlQuery = "SELECT COUNT(DISTINCT id) AS nombre_de_matchs " +
                "FROM rencontre " +
                "WHERE id_eq_dom = :idEquipe OR id_eq_ext = :idEquipe";

        Query query = entityManager.createNativeQuery(sqlQuery);
        query.setParameter("idEquipe", idEquipe);

        Object result = query.getSingleResult();

        // Conversion du résultat en int
        int nbMatchs = ((Number) result).intValue();

        return nbMatchs;
    }

}
