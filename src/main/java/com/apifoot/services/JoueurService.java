package com.apifoot.services;

import com.apifoot.dto.JoueurDto;
import com.apifoot.models.Joueur;
import com.apifoot.repositorys.JoueurRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Service CRUD pour la gestion des joueurs.
 */
@Service
@RequiredArgsConstructor
public class JoueurService {

    @Autowired
    private final JoueurRepository joueurRepository;
    @Autowired
    private final StatistiqueService statistiqueService;


    /**
     * Crée un nouveau joueur.
     *
     * @param joueur Le joueur à créer.
     * @return ResponseEntity<String> Indiquant le résultat de l'opération.
     */
    public ResponseEntity<String> creerJoueur(Joueur joueur){
        try {
            if (joueur.getId()==null){
                if (joueur.getAge()<16 || joueur.getAge()>50) {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                            .body("Un joueur doit avoir 16 ans minimum et 50 ans maximum");
                } else if (joueur.getPoids()<50 || joueur.getPoids()>120) {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                            .body("Un joueur doit peser 50 kg minimum et 120kg maximum.");
                } else if (joueur.getTaille()<1.50 || joueur.getTaille()>2.20) {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                            .body("Un joueur doit mesurer 1.50m minumum et 2.20m maximum.");
                } else {
                    joueur = joueurRepository.save(joueur);
                    JoueurDto joueurDto = new JoueurDto();
                    joueurDto.mapper(joueur);
                    return ResponseEntity.ok("Le joueur "+ joueur.getNomCourt() + " a été créé avec succès.");
                }
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Ce joueur a un ID existe déjà !");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Une erreur s'est produite lors de la création du joueur.");
        }
    }

    /**
     * Affiche les détails d'un joueur.
     *
     * @param id L'identifiant du joueur.
     * @return JoueurDto Les détails du joueur.
     */
    public JoueurDto afficherJoueur(Long id){
        Joueur joueur = joueurRepository.findById(id).orElse(null);
        if (joueur!=null){
            JoueurDto joueurDto = new JoueurDto();
            joueurDto.mapper(joueur);
            Integer nbButs = statistiqueService.countButsParJoueur(id);
            joueurDto.setNbButs(nbButs);
            Integer nbPasseD = statistiqueService.countPasseDparJoueur(joueur.getId());
            joueurDto.setNbPasseD(nbPasseD);
            Integer nbMatchs = statistiqueService.countMatchParJoueur(joueur.getEquipe().getId());
            joueurDto.setNbMatchs(nbMatchs);
            return joueurDto;
        }
        return null;
    }

    /**
     * Affiche la liste de tous les joueurs.
     *
     * @return List<JoueurDto> La liste des joueurs avec leurs statistiques.
     */
    public List<JoueurDto> afficherListeJoueurs(){
        List<Joueur> lesJoueurs = joueurRepository.findAll();
        List<JoueurDto> resultat = new ArrayList<>();
        for (Joueur joueur : lesJoueurs){
            JoueurDto joueurDto = new JoueurDto();
            joueurDto.mapper(joueur);
            Integer nbButs = statistiqueService.countButsParJoueur(joueur.getId());
            joueurDto.setNbButs(nbButs);
            Integer nbPasseD = statistiqueService.countPasseDparJoueur(joueur.getId());
            joueurDto.setNbPasseD(nbPasseD);
            Integer nbMatchs = statistiqueService.countMatchParJoueur(joueur.getEquipe().getId());
            joueurDto.setNbMatchs(nbMatchs);
            resultat.add(joueurDto);
        }
        return resultat;
    }

    /**
     * Modifie les informations d'un joueur existant.
     *
     * @param id     L'identifiant du joueur à modifier.
     * @param joueur Les nouvelles informations du joueur.
     * @return ResponseEntity<String> Indiquant le résultat de l'opération.
     */
    public ResponseEntity<String> modifierJoueur(Long id, Joueur joueur){
        try {
            Joueur newjoueur = joueurRepository.findById(id).orElse(null);
            if (newjoueur != null && joueur.getId() != null && joueur.getId()==newjoueur.getId()){
                if (joueur.getAge()<16 || joueur.getAge()>50) {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                            .body("Un joueur doit avoir 16 ans minimum et 50 ans maximum");
                } else if (joueur.getPoids()<50 || joueur.getPoids()>120) {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                            .body("Un joueur doit peser 50 kg minimum et 120kg maximum.");
                } else if (joueur.getTaille()< 1.50 || joueur.getTaille()> 2.20) {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                            .body("Un joueur doit mesurer 1.50m minumum et 2.20m maximum.");
                } else {
                    joueur.setId(id);
                    joueurRepository.save(joueur);
                    return ResponseEntity.ok("Le joueur "+ joueur.getNomCourt() + " a été modifié avec succès.");
                }
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Il se peut que ce joueur n'existe pas, vérifiez ID !");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Une erreur s'est produite lors de la modification du joueur.");
        }
    }


    /**
     * Supprime un joueur.
     *
     * @param id L'identifiant du joueur à supprimer.
     * @return ResponseEntity<String> Indiquant le résultat de l'opération.
     */
    public ResponseEntity<String> supprimerJoueur(Long id){
        try {
        joueurRepository.deleteById(id);
        return ResponseEntity.ok().build();
    } catch (Exception e) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Une erreur s'est produite lors de la suppression de l'élément.");
     }
    }

}
